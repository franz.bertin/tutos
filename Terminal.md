 2021-05-21

# De la ligne de commande

Depuis la nuit des temps informatiques, l'outil de base pour discuter avec sa machine est 
le "terminal", parfois aussi nommé l'XTerm ou la "ligne de commandes".

Il semble donc judicieux de maîtriser à minima ce type d'outils, quelque soit sa configuration et ses contraintes. Voici quelques points pour faire un état des lieux.
	
Vous lisez probablement ce texte en étant sur une machine Windows 10. 
<br>Toute polémique comparant Windows et Linux étant vaine, il faut apprendre à faire avec ce que l'on a.
<br>Et on a beaucoup.
	
	
## [L'XTerm](https://fr.wikipedia.org/wiki/Xterm#:~:text=xterm%20est%20l'%C3%A9mulateur%20de,environnement%20graphique%20X%20Window%20System.)

Quand Windows n'existait pas encore, 
<br>quand les informaticiens avaient des blouses blanches et des barbes grises, 
<br>l'XTerm était l'outil de base, une simple fenêtre monochrome permettant de passer des commandes ou de mettre ces commandes ensembles afin de faire des scripts géniaux.

Il n'y a pas "d'XTerm" sous Windows mais il y a [Putty](https://www.putty.org/), outil incontournable qui permet de se connecter à une machine Linux via une connexion SSH et donc d'avoir accès à une "XTerm".

Cette petite fenêtre permet aussi d'accéder à awk, sed, des outils toujours très utilisés car... excellent une fois qu'on les a compris.
	
* [awk](https://riptutorial.com/awk/example/21857/awk-by-examples "awk")
* [sed](https://www.grymoire.com/Unix/Sed.html "sed")
	
Au sein de la communauté, différentes sensibilités se sont exprimées et ainsi naquirent différentes versions du langage associé à l'XTerm: zsh, tcsh ksh... et bash.
	
* [Bash](https://riptutorial.com/bash) est maintenant la référence, un MUST

=> [Bash tutorial](https://www.javatpoint.com/bash)
	
	
## [Command Prompt - cmd](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands)

Le pendant de l'XTerm Linux sous Windows fut le "Command prompt" et savoir passer quelques commandes Windows dans une "fenêtre DOS" (autre dénomination pour Terminal Windows") est un savoir qui se perd et n'est plus réservé qu'à une "élite d'administrateurs expérimentés" afin de lancer des "batchs" pour mettre à jour des machines à distances, automatiquement.

C'est un vrai langage, avec une vrai syntaxe... Simple et bien documenté, il est néanmoins en passe d'être complètement obsolète.
	
Savoir écrire un batch (.bat) permet à moindre frais de faire du scripting. C'est gratuit et déjà installé sur toutes les machines Windows.
	
=> [Batch tutorial](https://www.instructables.com/Very-Basic-Batch-Tutorial/)


## [Powershell](https://docs.microsoft.com/en-us/powershell/scripting/overview?view=powershell-7.1)

Le Powershell est une version modernisée du "Command prompt" .
<br>Un administrateur Windows se doit de maîtriser le "Powershell" ; les autres resteront à l'écart... sauf nécessité.
	
=> [Powershell tutorial](https://www.guru99.com/powershell-tutorial.html)

## [Git Bash](https://gitforwindows.org/)

Git est la référence en matière d'outils de gestion de versions, quelque soit l'OS utilisé.

Un dévelopeur se doit de maîtriser Git ; les autres devraient.
<br>Il s'utilise essentiellement en mode "ligne de commande".
<br>Une fois installé, vous disposez d'un "terminal" permettant de passer les commandes Git mais aussi de nombreuses commandes Bash, 
donc de commencer à réaliser et tester des scripts qui fonctionneront aussi sous Linux.
	
Il est très astucieux de compléter les commandes bash avec quelques utilitaires récupérés sur internet et en déposant ses "binaires executables sous Windows" dans
C:\Program Files\Git\mingw64\bin
	
* [curl](https://curl.se/windows/ "curl")
* [wget](https://eternallybored.org/misc/wget/ "wget")
* [jq](https://stedolan.github.io/jq/download/ "jq")
		
	

## [Windows Subsystem for Linux - Wsl](https://docs.microsoft.com/en-us/windows/wsl/install-win10 "wsl")

WSL est une fonctionnalité récente (juin 2020) de Windows 10 permettant d'exécuter des binaires Linux.

D'un point de vue utilisateur, WSL permet d'accéder à une XTerm dans une fenêtre Windows et donc d'executer des applicatifs Linux... pour le moment sans interface graphique (mais cela est en train de changer: _[The Initial Preview of GUI app support is now available for the Windows Subsystem for Linux](https://devblogs.microsoft.com/commandline/the-initial-preview-of-gui-app-support-is-now-available-for-the-windows-subsystem-for-linux-2/)_).

Une fois activé WSL, il faut installer une distribution linux. Celles-ci sont disponibles (gratuitement) dans le Windows Store (_Kali_, _Ubuntu_, _Suse_, _Debian_...).
	
Tous les fichiers de la machine sont accessibles à la fois via Windows et via "Linux".

Dans l'une des distributions installées, on peut par exemple installer l'outil _Ansible_ qui permet d'installer n'importe quoi, n'importe où...
	
Cette fonctionnalité permet donc d'éviter de se connecter en SSH via _Putty_ à une machine Linux afin d'executer d'autres scripts.
<br>Utiliser moins de machines, c'est faire des économies.
	

## Les outils 

- [Putty](https://www.putty.org/ "Putty")

Outil de référence pour se connecter en SSH à une machine distante. Il est en général utile de l'installer avec d'autres outils tels que _[Pageant](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html "Pageant")_, _[Puttygen](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html "Puttygen")_ et _[WinSCP](https://winscp.net/eng/index.php "WinSCP")_

- [Windows Terminal](https://docs.microsoft.com/en-gb/windows/terminal/ "Windows Terminal")
	
C'est "un Must". **Windows Terminal** permet d'accéder simplement  aux différents interpréteurs de commande, avec multi-fenétrage, couleurs... 

Avec un même outil, vous pourrez donc passer aussi bien des commandes Windows que Linux et tout cela avec le sentiment d'utiliser un outil moderne.

_Disponible gratuitement dans le Microsoft Store_
		
- [Visual Studio Code](https://code.visualstudio.com/)
	
Le travail d'un informaticien est souvent de créer... 
Les outils pour créer furent regrouper sous le nom d'IDE (Integrated Development environment): _[Eclipse](https://www.eclipse.org/)_, _[Netbeans](https://netbeans.apache.org/)_, _[IntelliJ](https://www.jetbrains.com/idea/)_, _[PyCharm](https://www.jetbrains.com/fr-fr/pycharm/)_, _[Visual Studio](https://visualstudio.microsoft.com/fr/)_, _[Sublime Text](https://www.sublimetext.com/)_...

Chacun avec son look, ses spécificités, il en devient pénible de changer de langage.
Et là Microsoft créa "Visual Studio Code"... oui, n'oubliez pas le "**CODE**".

Cet IDE fonctionne à base de plugins et s'adapte donc à tout langage... mais pas uniquement.
Il dispose également de plugins _remarquables_:
		
* **Remote SSH** - Permet d'éditer un fichier en utilisant SSH. Vous pouvez donc utiliser un IDE moderne, en environnement graphique, sous windows, pour creer et modifier des fichiers sur une machine Linux
* **Remote WSL** - Permet d'éditer des fichiers dans votre propre environnement WSL
		
Visual Studio Code a de très nombreux plugins parmi lesquels:
		
* **Markdown**: Pour construire et gérer des fichiers au format "markdown" - ou comment faire de la doc rapidement
* **Docker**: pour construire et gérer ses applications conteneurisées

## Pour compliquer un peu

En complément à tout cela, l'utilisateur peut également utiliser [virtualbox](https://www.virtualbox.org/ "Virtualbox"). Ce logiciel permettra de créer une machine virtuelle sur son PC: par exemple, un Linux Ubuntu dans son Windows 10. Là encore, avec une unique machine, on peut profiter des bienfaits des 2 mondes.

Par exemple, uniquement sur sa propre machine, on pourra, sans perturber "la production":
* tester un script Ansible depuis sa fenêtre WSL vers n'importe quelle configuration Linux
* expérimenter, casser et recréer rapidement n'importe quelle configuration
* utiliser [Docker](https://www.docker.com/ "Docker") via WSL, comme sur une machine Linux, pour peu que [Docker Desktop](https://docs.docker.com/docker-for-windows/install/ "Docker Desktop") ait été installé. Cerise sur le gâteau, on dispose ainsi d'une Docker Registry sur son propre PC. Docker permettant de conteuneriser à peu près n'importe quelle application, cela ouvre le champ des possibles, uniquement avec sa machine.

---

Ce texte a pour vocation à mettre en évidence la richesse de ce qu'on peut faire avec une simple machine Windows 10 pour peu qu'on ait suffisament de mémoire et de CPU.

Le principal problème risque d'être maintenant de déterminer ce qu'on veut faire exactement...

Mais tout cela n'est qu'une vision personnelle :-)
