# Tutos

Tutos

# Git commands

git clone https://gitlab.com/franz.bertin/tutos.git

	( git remote add origin https://gitlab.com/franz.bertin/tutos.git )
	
cd tutos
<br>git config --local user.name "..."
<br>git config --local user.email "..."

Add file

<br>git status
<br>git add .
<br>git commit -m "Commentaire"
<br>git status

<br>git push -u origin master
...
<br>git push

# Using GPG for automatic commit signature

* Install [gnupg](https://www.gnupg.org/download/index.html) (windows / simple installer for the current GnuPG)
Software is installed in: C:\Program Files (x86)\GnuPG

* Follow [GitLab documentation](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html)
  
Using Git Bash

	gpg --full-gen-key

	gpg --list-secret-keys --keyid-format LONG <email>

---
<br>gpg: checking the trustdb
<br>gpg: marginals needed: 3  completes needed: 1  trust model: pgp
<br>gpg: depth: 0  valid:   2  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 2u
<br>sec   rsa4096/xxxxxxxxxxxxxxxxxxx 2021-05-21 [SC]
<br>      xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
<br>uid                 [ultimate] Franz Bertin <email>
<br>ssb   rsa4096/yyyyyyyyyyyyy 2021-05-21 [E]

---

	=> Passphrase: zzzzxxxx
	=> GPG key ID: XXXXXXXXXXXXXXX

	gpg --armor --export XXXXXXXXXXXXXXX

-----BEGIN PGP PUBLIC KEY BLOCK-----
...
-----END PGP PUBLIC KEY BLOCK-----

In Git repository

	git config --local user.signingkey XXXXXXXXXXXXXXXXXXX

	git config --local commit.gpgsign true